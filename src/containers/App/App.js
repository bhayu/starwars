import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import logo from './logo.svg';
import Login from '../../components/Tekion/Login';
import SearchBox from '../../components/Tekion/SearchBox';
import './App.css';

const propTypes = {
  userName: PropTypes.string
};

class App extends Component {
  render() {
    const {
      props: {
        userName
      }
    } = this;
    return (
      <div className="App">
        {<header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">{!userName ? 'Please login' : `Hi, ${userName}`}</h1>
        </header>}
        {!userName && <Login />}
        {userName && <SearchBox />}
      </div>
    );
  }
}

App.propTypes = propTypes;

function mapStateToProps (state) {
  const { login } = state;
  const { userName } = login;
  return {
    userName
  };
}

export default connect(mapStateToProps, null)(App);
