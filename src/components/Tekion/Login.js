import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as loginActions from '../../redux/reducers/login';
import ApiHelper from '../../helpers/ApiHelper';

const verificationFailedMessage = 'Oops !! Something went wrong, Please try again';
const invalidCredMessage = 'Invalid Credentials';
const peopleFirstUrl = 'https://swapi.co/api/people/?page=1';

const propTypes = {
	userName: PropTypes.string
};

class Login extends Component {
	constructor(props) {
		super(props);
		const methodNeeded = ['handleLogin', 'handleChange', 'checkAuth'];
		methodNeeded.forEach(mName => {
			this[mName] = this[mName].bind(this);
		});
		this.state = {
			uname: '',
			password: '',
			verifying: false
		}
	}
	checkAuth(id, password, next) {
		const {
			props: {
				actions:{
					setUser
				}
			}
		} = this;
		if (!next) {
			this.setState({ err: invalidCredMessage, verifying: false });
		} else {
			const apiClient = new ApiHelper();
			apiClient.get({apiEndPoint: next}).then(res => {
				if (typeof res === 'object' && Array.isArray(res.results)) {
					const { next: url, results } = res;
					const len = res.results.length;
					let userFound = false;

					for (let i = 0; i < len && !userFound; i++) {
						const user = results[i];
						if (user.name.toLowerCase() === id) {
							userFound = true;
							if (user.birth_year === password) {
								setUser(user.name);
								this.setState({ err: '', verifying: false });
							} else {
								this.setState({ err: invalidCredMessage, verifying: false });
							}
						}
					} // end of for loop
					if (!userFound) { // lets hope we get champ in result of next api call...
						this.checkAuth(id, password, url);
					}
				} else {
					this.setState({ err: verificationFailedMessage, verifying: false });
				}
			}).catch (err => {
				this.setState({ err: verificationFailedMessage, verifying: false });
			});
		}
	}
	handleLogin(e) {
		e.preventDefault();
		const {
			state: { uname, password },
			checkAuth
		} = this;
		if (uname && password) {
			this.setState({ err: '', verifying: true });
			checkAuth(uname.toLowerCase(), password, peopleFirstUrl);
		} else {
			this.setState({ err: 'Please provide valid user name and password' });
		}
	}

	handleChange(statePropName, value) {
		const { state } = this;
		state[statePropName] = value;
		this.setState(state);
	}
	render() {
		const {
			state: {
				password, uname, err, verifying
			},
			props: {
				userName
			},
			handleChange, handleLogin
		} = this;
		return(<div className="loginFormContainer">
			{err && <h4 className="error">{err}</h4>}
			{userName && <h1>Hello {userName}...</h1>}
			<form onSubmit={handleLogin}>
				<input
					className="inputText"
					type="text"
					name="uname"
					placeholder={"User Name"}
					value={uname}
					maxLength={25}
					onChange={(e) => handleChange('uname', e.target.value)}
				/>
				<input
					className="inputText"
					type="password"
					name="password"
					placeholder="*****"
					value={password}
					maxLength={10}
					onChange={(e) => handleChange('password', e.target.value)}
				/>
				<input
					className="inputBtn"
					type={verifying ? 'button' :'submit'} value={verifying ? 'Verifying...' : 'Login'}
					disabled={verifying}
				/>
			</form>
		</div>);
	}
}

Login.propTypes = propTypes;
Login.contextTypes = {
  store: PropTypes.object
};

function mapStateToProps (state) {
	const { login } = state;
	const { userName } = login;
	return {
		userName
	};
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(loginActions, dispatch)
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);
