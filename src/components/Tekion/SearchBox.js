import React, { Component } from 'react';

import ApiHelper from '../../helpers/ApiHelper';

const failureMessage = 'Oops !! Something went wrong while searching';
const planetFirstUrl = 'https://swapi.co/api/planets/?page=1';

let timerId;

class SearchBox extends Component {
	constructor(props) {
		super(props);
		const methodNeeded = ['handleChange', 'fetchPlanetsData'];
		methodNeeded.forEach(mName => {
			this[mName] = this[mName].bind(this);
		});
		this.state = {
			searchText: '',
			err: '',
			planets: []
		}
	}

	fetchPlanetsData(next = planetFirstUrl) {
		if (next) {
			const {
				state: {
					planets,
					searchText
				}
			} = this;
			const lowerSearchText = searchText.toLowerCase();

			const apiClient = new ApiHelper();
			apiClient.get({apiEndPoint: next}).then(res => {
				if (typeof res === 'object' && Array.isArray(res.results)) {
					const { next: nextUrl, results } = res;
						results.forEach(planet => {
							if (planet.name.toLowerCase().indexOf(lowerSearchText) !== -1) {
								planets.push(planet);
							}
						});
					this.setState({ planets })
					this.fetchPlanetsData(nextUrl);
				} else {
					this.setState({ err: failureMessage });
				}
			}).catch (err => {
				this.setState({ err: failureMessage });
			});
		}
	}


	handleChange(e) {
		const { value } = e.target;
		const { fetchPlanetsData } = this;
		this.setState({
			err: '',
			planets: [],
			searchText: value
		});
		clearTimeout(timerId);
    if (value && value.length >= 2) {
      timerId = setTimeout(fetchPlanetsData, 500);
    }
	}
	render() {
		const {
			state: {
				searchText, planets, err
			},
			handleChange
		} = this;
		const planetsData = [];
		if (Array.isArray(planets) && planets.length > 0) {
			planets.forEach((planet, index) => {
				planetsData.push((<li className="planetResult" key={`planet_${index}`}>
					{ planet.name }
				</li>));
			});
		}

		return(<div className="searchContainer">
			{err && <h4 className="error">{err}</h4>}
			<input
				type="text"
				name="searchbox"
				className="inputText inputSearchTxt"
				placeholder={"Search planets here"}
				value={searchText}
				maxLength={25}
				onChange={(e) => handleChange(e)}
			/>

			<ul>{planetsData}</ul>
		</div>);
	}
}

export default SearchBox;
