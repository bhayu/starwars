import superagent from 'superagent';

class ApiClient {
  constructor() {
    const methods = ['get', 'post', 'put', 'del'];

    methods.forEach((method) =>
      this[method] = (obj) => new Promise((resolve, reject) => {
        const { params, data, headers, apiEndPoint, timeLimit } = obj;
        const request = superagent[method](apiEndPoint);
        if (params) {
          request.query(params);
        }
        if (headers) {
          request.set(headers);
        }
        if (data) {
          request.send(data);
        }
        // default timeout limit set to 10 seconds for each api
        request.timeout(timeLimit || 10000);
        request.end((err, { body, text } = {}) => {
          if (err) {
            reject(body || err);
          } else {
            resolve(body || text);
          }
        });
      }));
  }
}

export default ApiClient;
