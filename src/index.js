import React from 'react';
import App from './containers/App/App.js';
import { render } from 'react-dom';
import configureStore from './redux/store/configureStore';
import { Provider } from 'react-redux';
const store = configureStore();

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);