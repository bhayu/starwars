/* redux stuffs starts */
const SET_USER   = 'SET_USER';


const initialState = {
  userName: ''
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
      case SET_USER:
        return {
            ...state,
            userName: action.userName
        };
      default:
        return state;
    }
};

export const setUser = (userName) => ({
    type: SET_USER,
    userName
});
